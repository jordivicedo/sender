package com.otalio.kafkasender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SenderController {

    @Autowired
    private Sender sender;
    
    @RequestMapping("/statistics")
    public Statistics statistics() {
        return sender.getStatistics();
    }
    
}

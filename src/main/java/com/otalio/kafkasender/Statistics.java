package com.otalio.kafkasender;

public class Statistics {
    private final int total;
    private final int errors;
    
    public Statistics(int total, int errors) {
        this.total = total;
        this.errors = errors;
    }
    
    public int getTotal() {
        return total;
    }
    
    public int getErrors() {
        return errors;
    }

}

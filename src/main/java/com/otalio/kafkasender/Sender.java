package com.otalio.kafkasender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Sender {
    public static Logger logger = LoggerFactory.getLogger(Sender.class);
    
    @Value("${sender.max_effective_queue_size}")
    private Integer maxEffectiveQueueSize;
    
    @Value("${kafka.origin-servers}")
    private String originBootstrapAddress;

    @Value("${kafka.destination-servers}")
    private String destinationBootstrapAddress;

    @Value("#{'${kafka.topics}'.split(';')}")
    private List<String> topicList; // t1;t2;t3;t4;t5

    @Value("${sender.group_id}")
    private String groupId;
    
    @Value("${sender.poll_wait}")
    private Integer pollWait;
    
    /**
     * Get consumer subscribed to the topic
     */
    private Map<String, Consumer<String, String>> consumersPerTopic = new HashMap<>();
    
    private Queue<RecordData> queue = new LinkedList<>();
    private List<Consumer<String, String>> consumers;
    private Producer<String, String> producer;
    
    private AtomicInteger errors = new AtomicInteger(0);
    private AtomicInteger total = new AtomicInteger(0);
    
    @PostConstruct
    public void init() {
        consumers = getConsumers();
        producer = getProducer();
    }
    
    @Async
    public void run() {
        while (true) {
            fillQueueFromOriginKafka();
            sendDataInQueue();
            removeSentMessagesAndChangeOffset();
        }
    }

    private void removeSentMessagesAndChangeOffset() {
        List<RecordData> recordsToBeRemoved = new ArrayList<>();
        for (String topic : topicList) {
            List<RecordData> records = getRecordsForTopic(topic);
            RecordData lastSentRecord = null;
            
            for (RecordData record :records) {
                if (record.getStatus() == RecordDataStatus.SENT) {
                    lastSentRecord = record;
                    recordsToBeRemoved.add(record);
                } else {
                    break;
                }
            }
            
            if (lastSentRecord != null) {
                commitRecord(lastSentRecord);
            }
        }
        
        queue.removeAll(recordsToBeRemoved);
    }

    private List<RecordData> getRecordsForTopic(String topic) {
        return queue.stream().filter(recordData -> recordData.getRecord().topic().equals(topic)).collect(Collectors.toList());
    }

    private void commitRecord(RecordData lastSentRecord) {
        ConsumerRecord<String, String> record = lastSentRecord.getRecord();
        String topic = record.topic();
        long offset = record.offset();
        TopicPartition topicPartition = new TopicPartition(topic, record.partition());
        OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(offset + 1);
        getConsumerFor(topic).commitSync(Collections.singletonMap(topicPartition, offsetAndMetadata));
    }

    private void fillQueueFromOriginKafka() {
        for (Consumer<String, String> currentConsumer : consumers) {
            if (getEffectiveQueueSize() < maxEffectiveQueueSize) {
                ConsumerRecords<String, String> data = currentConsumer.poll(pollWait);

                Iterator<ConsumerRecord<String, String>> iterator = data.iterator();

                while (iterator.hasNext()) {
                    ConsumerRecord<String, String> record = iterator.next();
                    RecordData recordData = new RecordData(record);
                    queue.add(recordData);
                }
                
                if (data.count() > 0) {
                    // Once we get data, break and try to send it
                    break;
                }
            } else {
                // If the queue is full, start from the beginning again
                break;
            }
        }
    }

    private long getEffectiveQueueSize() {
        return queue.stream().filter(recordData -> recordData.getStatus() != RecordDataStatus.SENT).count();
    }

    
    private void sendDataInQueue() {
        for (RecordData recordData : queue) {
            if (recordData.getStatus() == RecordDataStatus.ERROR ||  recordData.getStatus() == RecordDataStatus.WAITING_FOR_SEND) {
                total.incrementAndGet();
                //logger.info("Sending message from {}", recordData.getRecord().topic());
                ProducerRecord<String, String> producerRecord = getProducerRecord(recordData);
                recordData.setStatus(RecordDataStatus.SENDING);
                producer.send(producerRecord, new Callback() {
                    
                    @Override
                    public void onCompletion(RecordMetadata metadata, Exception exception) {
                        if (exception == null) {
                            recordData.setStatus(RecordDataStatus.SENT);
                        } else {
                            recordData.setStatus(RecordDataStatus.ERROR);
                            errors.incrementAndGet();
                        }
                    }
                });
            }
        }
    }

    private ProducerRecord<String, String> getProducerRecord(RecordData recordData) {
        ConsumerRecord<String, String> record = recordData.getRecord();
        return new ProducerRecord<String, String>(record.topic(), record.key(), record.value());
    }

    private Consumer<String, String> getConsumerFor(String topic) {
        return consumersPerTopic.get(topic);
    }

    private Producer<String, String> getProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, destinationBootstrapAddress);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, maxEffectiveQueueSize);
        props.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "gzip");
        
        Producer<String, String> producer = new KafkaProducer<String, String>(props);
        return producer;
    }

    private List<Consumer<String, String>> getConsumers() {
        List<Consumer<String, String>> consumers = new ArrayList<Consumer<String, String>>();

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, originBootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxEffectiveQueueSize);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

        for (String singleTopic : topicList) {
            logger.info("Creating consumer for {}", singleTopic);

            Consumer<String, String> consumer = new KafkaConsumer<>(props);

            List<TopicPartition> partitions = new ArrayList<>();
            for (PartitionInfo partition : consumer.partitionsFor(singleTopic)) {
                TopicPartition topicPartition = new TopicPartition(singleTopic, partition.partition());
                partitions.add(topicPartition);
            }
            
            consumersPerTopic.put(singleTopic, consumer);
            consumer.assign(partitions);

            consumers.add(consumer);
        }

        return consumers;
    }

    public Statistics getStatistics() {
        return new Statistics(total.intValue(), errors.intValue());
    }
    
}

package com.otalio.kafkasender;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public class RecordData {
    private RecordDataStatus status;
    private ConsumerRecord<String, String> record;

    public RecordData(ConsumerRecord<String, String> record) {
        this.record = record;
        this.status = RecordDataStatus.WAITING_FOR_SEND;
    }
    
    public synchronized RecordDataStatus getStatus() {
        return status;
    }

    public synchronized void setStatus(RecordDataStatus status) {
        this.status = status;
    }
    
    public void setRecord(ConsumerRecord<String, String> record) {
        this.record = record;
    }
    
    public ConsumerRecord<String, String> getRecord() {
        return record;
    }
    

}

package com.otalio.kafkasender;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SenderInitializer {
    
    @Autowired
    private Sender sender;
    
    @PostConstruct
    public void run() {
        sender.run();
    }

}

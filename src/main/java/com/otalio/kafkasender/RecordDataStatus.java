package com.otalio.kafkasender;

public enum RecordDataStatus {
    WAITING_FOR_SEND, SENDING, SENT, ERROR;
}
